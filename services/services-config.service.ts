import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root',
})
export class ServicesConfigService {
  servicesConfig;

  constructor() {
    this.servicesConfig = {
      cleaning: {
        name: 'cleaning'
      },
      electrical: {
        mainImage: 'assets/images/electrical/main-electrical.png',
        name: 'Electrical',
        rating: 4,
        totalReviews: 234,
        description: 'When you have a electrical issue that needs professional attention, look no further\n' +
          'than Hunt Handy. We will connect you with most experience and affordable electrician\n' +
          'in your area  and to give you more peace of mind we wont pay them till you approve\n' +
          'the job you hire them to do it.',
        details: [
          {
            image: 'assets/images/electrical/01.png',
            text:  'Our Electrician is fully licensed and insured.'
          },
          {
            image: 'assets/images/electrical/02.png',
            text: 'Hunt Handy will bring you electrician can\n' +
              'handle the big jobs – Installing wiring,\n' +
              'lighting, and control systems in new and\n' +
              'existing buildings, according to\n' +
              'municipal codes.'
          },
          {
            image: 'assets/images/electrical/03.png',
            text: 'Hunt Handy will connect you with electrician\n' +
              'are remain up to date on all the latest, most\n' +
              'advanced, electrical techniques such as\n' +
              'Reading and interpreting architect blueprints\n' +
              '&amp; circuit diagrams.'
          },
          {
            image: 'assets/images/electrical/04.png',
            text: 'Hunt Handy develops cost effective solutions\n' +
              'and provide you with an accurate estimate\n' +
              'before work begins – in other words, no\n' +
              'surprise overtime charges! We will assure to\n' +
              'get best Experience and price affordable in\n' +
              'the market.'
          }
        ],
        endDetails: 'Hunt Handy is not an employer, but simply connects independent  service professionals / Contractor  with customers.'
      },
      plumber: {
         mainImage: 'assets/images/plumbing/main-plumbing.png',
        name: 'Plumber',
        rating: 4,
        totalReviews: 234,
        description:  'When you have a plumbing issue that needs professional attention, look no further\n' +
          'than Hunt Handy. We will connect you with most experience and affordable Plumber\n' +
          'in your area  and to give you more peace of mind we wont pay them till you approve\n' +
          'the job you hire them to do it, to release the fund for them.',
        details: [
          {
            image: 'assets/images/plumbing/01.png',
            text:  'Our Plumber is fully licensed and insured.'
          },
          {
            image: 'assets/images/plumbing/02.png',
            text:  'Hunt Handy will bring you plumber can\n' +
              'handle the big jobs – like basement\n' +
              'waterproofing and lowering.'
          },
          {
            image: 'assets/images/plumbing/03.png',
            text: 'Hunt Handy will connect you with Plumber\n' +
              'are remain up to date on all the latest, most\n' +
              'advanced, plumbing techniques such as\n' +
              'camera inspection.'
          },
          {
            image: 'assets/images/plumbing/04.png',
            text: 'Hunt Handy develops cost effective solutions\n' +
              'and provide you with an accurate estimate\n' +
              'before work begins – in other words, no\n' +
              'surprise overtime charges! We will assure to\n' +
              'get best Experience and price affordable in\n' +
              'the market.'
          }
        ],
        endDetails: 'Hunt Handy is not an employer, but simply connects independent  service professionals / Contractor  with customers.'
      },
      handyman: {
        name: 'Handyman'
      },
      painting: {
        mainImage: 'assets/images/painting/painting.png',
        name: 'Painting',
        rating: 4,
        totalReviews: 234,
        description: 'Are you looking to paint your house / Office , reliable and the best at what they do?\n' +
          'If you want to get the best results with minimal fuss, then make hunt Handy your\n' +
          'first choice we will connect you with expert painting contractors. And professional\n' +
          'in what they are doing, Hunt Handy will put you in touch with the right people for\n' +
          'the job.',
        details: [
          {
            image: 'assets/images/painting/01.png',
            text: 'Need to paint your home / office or even\n' +
              'one wall no problem let Hunt Handy\n' +
              'connect you with  most experience\n' +
              'painter in the market.  '
          },
          {
            image: 'assets/images/painting/02.png',
            text: 'All our professional are background\n' +
              'check to make sure you who is coming\n' +
              'to your home.'
          },
          {
            image: 'assets/images/painting/03.png',
            text: 'Hunt Handy Professional are experienced\n' +
               'and well trained. '
          },
          {
            image: 'assets/images/painting/04.png',
            text: 'Move in or Move out dont worry ask\n' +
              'Hunt Handy to connect you with\n' +
              'best Painter in market. '
          }
        ],
        endDetails: 'Hunt Handy is not an employer, but simply connects independent  service professionals / Contractor  with customers.'
      },
      'pest-control': {
        name: 'Pest Control'
      }
    };
  }

  public getServiceConfig(serviceName) {
    return this.servicesConfig[serviceName];
  }

}
