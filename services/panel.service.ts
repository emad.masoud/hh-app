import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import { ApiService } from './api.service';

import {User, UserProfile} from '../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class PanelService {

  constructor(
    private apiService: ApiService,
    private toastrService: ToastrService
  ) {}

  public fetchContractorDetails(): Observable<any> {
    return this.apiService.get('profile')
      .pipe(map(res => {
        return res.data;
      }));
  }

  public saveExperienceCertificates(params): Observable<any> {
    return this.apiService.put('contractor/experience', params)
      .pipe(map(res => {
        this.toastrService.success( 'Certificates Saved Successfully.');
        return res.data;
      }));
  }

  public saveAbout(params): Observable<any> {
    return this.apiService.put('contractor/bio', params)
      .pipe(map(res => {
        this.toastrService.success( 'Bio Saved Successfully.');
        return res.data;
      }));
  }

  public fetchRatings(): Observable<any> {
    return this.apiService.get('service/ratings')
      .pipe(map(res => {
        return res.data.ratingsFound;
      }));
  }

  public fetchPastJobList(): Observable<any> {
    return this.apiService.get('service/past/jobs/list')
      .pipe(map(res => {
        return res.data.pastJobs;
      }));
  }

  public fetchEarnings(): Observable<any> {
    return this.apiService.get('contractor/earnings')
      .pipe(map(res => {
        return res.data;
      }));
  }

  public fetchWithdrawals(): Observable<any> {
    return this.apiService.get('stripe/connected-accounts/withdrawals')
      .pipe(map(res => {
        return res.data.withdrawals;
      }));
  }

  public fetchTaskDetails(taskId): Observable<any[]> {
    return this.apiService.get('services/' + taskId)
      .pipe(map(res => {
        return res.data.job;
      }));
  }

  public fetchUpComingJobList(): Observable<any> {
    return this.apiService.get('service/appointments/list')
      .pipe(map(res => {
        return res.data.upcomingJobs;
      }));
  }

  public fetchFaqs(): Observable<any> {
    return this.apiService.get('configurations/faqs')
      .pipe(map(res => {
        return res.data.faqs;
      }));
  }

  public fetchTasksCalender(): Observable<any> {
    return this.apiService.get('services/calender/info')
      .pipe(map(res => {
        return res.data;
      }));
  }

}
