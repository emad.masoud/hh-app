import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { environment } from '../../environments/environment';
import { Data, Response } from '../models/api.model';

@Injectable({
  providedIn: 'root',
})
export class ApiService {

  private apiUrl: string = environment.apiUrl;

  constructor(
    private httpClient: HttpClient,
    private router: Router,
    private toastrService: ToastrService
  ) {}

  public get(path: string, params: HttpParams = new HttpParams()): Observable<Data> {
    return this.httpClient.get<Response>(this.apiUrl + path, { params })
      .pipe(map(res => {
        return {
          data: res.data
        };
      }), catchError(errorResponse => {
        return this.handleError(errorResponse);
      }));
  }

  public post(path: string, params: object = {}): Observable<Data> {
    return this.httpClient.post<Response>(this.apiUrl + path, params)
      .pipe(map(res => {
        return {
          data: res.data
        };
      }), catchError(errorResponse => {
        return this.handleError(errorResponse);
      }));
  }

  public put(path: string, params: object = {}): Observable<Data> {
    return this.httpClient.put<Response>(this.apiUrl + path, params)
      .pipe(map(res => {
        return {
          data: res.data
        };
      }), catchError(errorResponse => {
        return this.handleError(errorResponse);
      }));
  }

  public patch(path: string, params: object = {}): Observable<Data> {
    return this.httpClient.patch<Response>(this.apiUrl + path, params)
      .pipe(map(res => {
        return {
          data: res.data
        };
      }), catchError(errorResponse => {
        return this.handleError(errorResponse);
      }));
  }

  public uploadFile(path: string, fileToUpload: File): Observable<Data> {
    const fd = new FormData();
    fd.append('image', fileToUpload);
    return this.httpClient.post<Response>(this.apiUrl + path, fd)
      .pipe(map(res => {
        return {
          data: res.data
        };
      }), catchError(errorResponse => {
        return this.handleError(errorResponse);
      }));
  }

  private handleError(errorResponse: HttpErrorResponse) {
    if (errorResponse.error instanceof ErrorEvent) {
      console.log('Client Side Error', errorResponse);
    } else {
      console.log('Server Side Error', errorResponse);
      if (errorResponse.status) {
        this.toastrService.error(errorResponse.error.message);
        if (errorResponse.status === 401) {
          localStorage.removeItem('user');
          this.router.navigate(['/login']);
        }
      } else {
        this.toastrService.error('Server is not responding');
      }
      return throwError(errorResponse.error);
    }
  }
}
