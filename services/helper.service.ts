import { Injectable } from '@angular/core';
import { Router } from "@angular/router";

@Injectable({
  providedIn: 'root',
})
export class HelperService {

  constructor(
    private router: Router
  ) {}

  handleContractorSignUpSteps(step) {
    const signUpData = JSON.parse(localStorage.getItem('contractorSignUp'));

    if (!signUpData || !signUpData.stepCompleted) {
      return this.router.navigate(['/contractor/sign-up/personal-info']);
    }

    if (signUpData.stepCompleted < (step - 1)) {
      const goToStep = signUpData.stepCompleted + 1;
      if (goToStep === 1) {
        return this.router.navigate(['/contractor/sign-up/personal-info']);
      } else if (goToStep === 2) {
        return this.router.navigate(['/contractor/sign-up/work-info']);
      } else if (goToStep === 3) {
        return this.router.navigate(['/contractor/sign-up/other-info']);
      } else if (goToStep === 4) {
        return this.router.navigate(['/contractor/sign-up/address-info']);
      } else if (goToStep === 5) {
        return this.router.navigate(['/contractor/sign-up/id-info']);
      } else if (goToStep === 6) {
        return this.router.navigate(['/contractor/sign-up/services-info']);
      }
    }
  }

  formatDate(unixDate) {
    const months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
    const date = new Date(unixDate * 1000);
    return date.getDate() + '-' + months[date.getMonth()] + '-' + date.getFullYear();
  }

  formatTime(unixDate) {
    const date = new Date(unixDate * 1000);
    let hours = date.getHours(),
      minutes = date.getMinutes(),
      ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    const newMinutes = minutes < 10 ? '0'+ minutes : minutes,
      strTime = hours + ':' + newMinutes + ' ' + ampm;
    return strTime;
  }

  validatePhoneNumber(phoneNumber) {
    const regexCanada = /^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/;
    const regexPakistan = /^((\+92)|(0092)|(92))-{0,1}\d{3}-{0,1}\d{7}$|^\d{11}$|^\d{4}-\d{7}$/;
    return regexPakistan.test(phoneNumber) || regexCanada.test(phoneNumber);
  }
}
