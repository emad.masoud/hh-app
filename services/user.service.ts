import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router } from "@angular/router";

import { ApiService } from './api.service';

import {User, UserProfile} from '../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class UserService {

  private loggedIn = new BehaviorSubject<User>(null);

  constructor(
    private apiService: ApiService,
    private toastrService: ToastrService,
    private router: Router
  ) {
    this.loggedIn.next(this.getUser());
  }

  public login(params): Observable<User> {
    return this.apiService.post('common/login', params)
      .pipe(map(res => {
        this.toastrService.success( 'Logged In Successfully.');
        this.setUser(res.data.user);
        return res.data.user;
      }));
  }

  public user(): Observable<User> {
    return this.apiService.get('common/account')
      .pipe(map(res => {
        this.toastrService.success( 'Already logged in.');
        return res.data.user;
      }));
  }

  public isLoggedIn() {
    return this.loggedIn.asObservable();
  }

  public logout() {
    this.toastrService.success( 'Logged Out Successfully.');
    this.removeUser();
    return this.router.navigate(['/']);
  }

  public setUser(user) {
    const userData = {
      _id: user._id,
      name: user.firstName + ' ' + user.lastName,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      phoneNumber: user.phoneNumber,
      profileImage: user.profileImage,
      userType: user.userType
    };
    localStorage.setItem('user', JSON.stringify(userData));
    this.loggedIn.next(userData);
  }

  public getUser(): User {
    return JSON.parse(localStorage.getItem('user'));
  }

  public removeUser() {
    localStorage.removeItem('user');
    this.loggedIn.next(null);
  }

  public userSignUp(params): Observable<User> {
    return this.apiService.post('user/signup', params)
      .pipe(map(res => {
        this.toastrService.success( 'User Registered Successfully.');
        this.setUser(res.data);
        return res.data;
      }));
  }

  public fetchUserProfile(): Observable<UserProfile> {
    return this.apiService.get('user/profile')
      .pipe(map(res => {
        return res.data;
      }));
  }

  public saveUserProfile(params): Observable<User> {
    return this.apiService.patch('profile', params)
      .pipe(map(res => {
        this.toastrService.success( 'User Profile Saved Successfully.');
        const user = this.getUser();
        Object.assign(user, params);
        this.setUser(user);
        return res.data;
      }));
  }

  public fetchCurrentBookings(): Observable<any[]> {
    return this.apiService.get('services/customer/jobs/list')
      .pipe(map(res => {
        return res.data.currentJobs;
      }));
  }

  public fetchPastBookings(): Observable<any[]> {
    return this.apiService.get('services/customer/past/jobs/list')
      .pipe(map(res => {
        return res.data.pastJobs;
      }));
  }

  public uploadImage(file): Observable<any> {
    return this.apiService.uploadFile('upload/image', file)
      .pipe(map(res => {
        return res.data;
      }));
  }

  public fetchServices(): Observable<any[]> {
    return this.apiService.get('services')
      .pipe(map(res => {
        return res.data.servicesFound;
      }));
  }

  public contractorSignUp(params): Observable<User> {
    return this.apiService.post('contractor/signup', params)
      .pipe(map(res => {
        this.toastrService.success( 'Contractor Registered Successfully.');
        this.setUser(res.data);
        return res.data;
      }));
  }

  public fetchBookingDetails(bookingId): Observable<any[]> {
    return this.apiService.get('services/' + bookingId)
      .pipe(map(res => {
        return res.data.job;
      }));
  }

}
