import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Router } from '@angular/router';

import { UserService } from '../../services/user.service';

import { User } from '../../models/user.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit, OnDestroy {

  navbarOpen = false;
  loggedIn$: Observable<User>;
  subscription: Subscription;
  user: User = null;

  constructor(
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit() {
    this.loggedIn$ = this.userService.isLoggedIn();
    this.subscription = this.loggedIn$.subscribe(res => {
      this.user = res;
    });
  }

  toggleNavbar(){
    this.navbarOpen = !this.navbarOpen;
  }

  goToProfile() {
    const userData = JSON.parse(localStorage.getItem('user'));
    let route = ['/'];
    if (userData.userType === 'user') {
      route = ['/user/profile'];
    } else {
      route = ['/contractor/panel/home'];
    }
    this.router.navigate(route);
  }

  logout() {
    return this.userService.logout();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
