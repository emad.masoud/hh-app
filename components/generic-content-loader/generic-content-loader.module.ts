import { NgModule } from '@angular/core';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import { GenericContentLoaderComponent } from './generic-content-loader.component';


@NgModule({
  declarations: [
    GenericContentLoaderComponent
  ],
  imports: [
    NgxSkeletonLoaderModule
  ],
  exports: [
    GenericContentLoaderComponent
  ]
})
export class GenericContentLoaderModule { }
