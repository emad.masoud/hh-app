import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from '@angular/router';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {GenericContentLoaderModule} from "../../../components/generic-content-loader/generic-content-loader.module";

import { ProfileComponent } from "./profile.component";
import { BookingDetailsComponent } from "./booking-details/booking-details.component";

const routes: Routes = [
  {
    path: '',
    component: ProfileComponent
  },
  {
    path: 'booking-details/:bookingId',
    component: BookingDetailsComponent
  }
];

@NgModule({
  declarations: [
    ProfileComponent,
    BookingDetailsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    GenericContentLoaderModule,
    RouterModule.forChild(routes)
  ]
})
export class ProfileModule { }
