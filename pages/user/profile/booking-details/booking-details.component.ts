import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { } from 'googlemaps';
import {ActivatedRoute} from "@angular/router";
import {UserService} from "../../../../services/user.service";
import {Subscription} from "rxjs";
import {HelperService} from "../../../../services/helper.service";

@Component({
  selector: 'app-booking-details',
  templateUrl: './booking-details.component.html'
})
export class BookingDetailsComponent implements OnInit, OnDestroy {
  @ViewChild('gmap') gmapElement: any;
  map: google.maps.Map;

  bookingId: string;
  bookingDetails: any;
  subscription: Subscription;
  isLoadingBookingDetails = true;

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private helperService: HelperService
  ) {}

  ngOnInit() {
    this.bookingId = this.route.snapshot.paramMap.get('bookingId');
    this.subscription = this.userService.fetchBookingDetails(this.bookingId).subscribe(res => {
      this.bookingDetails = res;
      this.isLoadingBookingDetails = false;
      const mapProp = {
        center: new google.maps.LatLng(this.bookingDetails.latitude, this.bookingDetails.longitude),
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);
    });
  }

  formatDate(unixDate) {
    return this.helperService.formatDate(unixDate);
  }

  formatTime(unixDate) {
    return this.helperService.formatTime(unixDate);
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
