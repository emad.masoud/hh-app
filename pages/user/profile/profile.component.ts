import { Component, OnDestroy, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import { forkJoin, Subscription } from 'rxjs';

import { UserService } from '../../../services/user.service';
import { Router } from '@angular/router';
import {HelperService} from "../../../services/helper.service";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls : ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit, OnDestroy {

  isLoadingData = true;
  isValidPhoneNumber = true;
  profileFormSubmit = false;
  uploadingImage = false;
  profileForm = new FormGroup({
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    phoneNumber: new FormControl('', Validators.required),
    aptNumber: new FormControl('', Validators.required),
    streetAddress: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required),
    province: new FormControl('', Validators.required),
    postalCode: new FormControl('', Validators.required),
  });
  userData = null;

  currentBookings = [];
  pastBookings = [];

  subscription: Subscription;
  uploadImageSubscription: Subscription;

  constructor(
    private router: Router,
    private userService: UserService,
    private helperService: HelperService
  ) {}

  ngOnInit() {
    forkJoin(
      this.userService.fetchUserProfile(),
      this.userService.fetchCurrentBookings(),
      this.userService.fetchPastBookings()
    ).subscribe(res => {
      const profile = res[0];
      this.currentBookings = res[1];
      this.pastBookings = res[2];
      this.profileForm.setValue({
        firstName: profile.firstName,
        lastName: profile.lastName,
        email: profile.email,
        phoneNumber: profile.phoneNumber,
        aptNumber: profile.aptNumber,
        streetAddress: profile.streetAddress,
        city: profile.city,
        province: profile.province,
        postalCode: profile.postalCode
      });
      this.userData = profile;
      this.profileForm.get('email').disable();
      this.isLoadingData = false;
    });
  }

  saveProfile() {
    this.profileFormSubmit = true;
    if (this.profileForm.invalid) { return; }
    this.subscription = this.userService.saveUserProfile(this.profileForm.value).subscribe(res => {
      //return this.router.navigate(['/']);
    });
  }

  get fp() { return this.profileForm.controls; }

  validatePhoneNumber(phoneNumber) {
    if (this.helperService.validatePhoneNumber(phoneNumber)) {
      this.isValidPhoneNumber = true;
      return true;
    } else {
      this.isValidPhoneNumber = false;
      return false;
    }
  }

  formatDate(unixDate) {
    return this.helperService.formatDate(unixDate);
  }

  uploadProfileImage(event) {
    if (event.target.files && event.target.files.length) {
      this.uploadingImage = true;
      const image = event.target.files[0];
      this.uploadImageSubscription = this.userService.uploadImage(image).subscribe(res => {
        this.userService.saveUserProfile({profileImage: res.url}).subscribe(result => {
          this.userData.profileImage = res.url;
          this.uploadingImage = false;
        });
      });
    }
  }
  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.uploadImageSubscription) {
      this.uploadImageSubscription.unsubscribe();
    }
  }

}
