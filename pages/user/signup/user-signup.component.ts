import {Component, OnDestroy, OnInit} from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import {FormGroup, FormControl, Validators} from '@angular/forms';


import { User, UserRegister } from '../../../models/user.model';
import { UserService } from '../../../services/user.service';


@Component({
  selector: 'app-user-signup',
  templateUrl: './user-signup.component.html'
})
export class UserSignupComponent implements OnInit, OnDestroy {

  userFormSubmit = false;
  subscription: Subscription;

  userForm = new FormGroup({
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    phoneNumber: new FormControl('', Validators.required),
    password: new FormControl('', [Validators.required, Validators.minLength(8)]),
    confirmPassword: new FormControl('', Validators.required),
    referralCode: new FormControl('')
  }, this.pwdMatchValidator);

  constructor(
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit() {

  }

  userSignUp() {
    this.userFormSubmit = true;
    if (this.userForm.invalid) { return; }
    this.subscription = this.userService.userSignUp(this.userForm.value).subscribe((res: User) => {
      return this.router.navigate(['/']);
    });
  }

  get f() { return this.userForm.controls; }

  pwdMatchValidator(frm: FormGroup) {
    return frm.get('password').value === frm.get('confirmPassword').value
      ? null : { mismatch: true };
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
