import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { UserAuthGuard } from "../../guards/userAuth.guard";

const routes: Routes = [
  {
    path: 'sign-up',
    loadChildren: './signup/user-signup.module#UserSignupModule'
  },
  {
    path: 'profile',
    loadChildren: './profile/profile.module#ProfileModule',
    canActivate: [ UserAuthGuard ]
  }
];

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class UserModule { }
