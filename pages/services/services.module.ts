import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { ServiceDetailsComponent } from './service-details/service-details.component';
import { ListServicesComponent } from './list-services/list-services.component';
import {GetEstimationComponent} from "./get-estimation/get-estimation.component";
import {FindersListComponent} from "./finders-list/finders-list.component";
import {ContractorDetailsComponent} from "./contractor-details/contractor-details.component";


const routes: Routes = [
  { path: '', component: ListServicesComponent },
  { path: ':serviceName', component: ServiceDetailsComponent },
  { path: 'get-estimation/:serviceId', component: GetEstimationComponent },
  { path: 'finders-list/:serviceId', component: FindersListComponent },
  { path: 'contractor-details/:contractorId', component: ContractorDetailsComponent },
];

@NgModule({
  declarations: [
    ListServicesComponent,
    ServiceDetailsComponent,
    GetEstimationComponent,
    FindersListComponent,
    ContractorDetailsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class ServicesModule {}
