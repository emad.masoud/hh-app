import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ServicesConfigService } from '../../../services/services-config.service';

@Component({
  selector: 'app-service-details',
  templateUrl: './service-details.component.html',
  styleUrls : ['./service-details.component.scss']
})
export class ServiceDetailsComponent implements OnInit {

  serviceDetails: object;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private servicesConfig: ServicesConfigService
  ) {}

  ngOnInit() {
    this.serviceDetails = this.servicesConfig.getServiceConfig(this.route.snapshot.paramMap.get('serviceName'));
    if (!this.serviceDetails) {
      return this.router.navigate(['/services']);
    }
  }

}
