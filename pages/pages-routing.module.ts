import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PagesComponent } from './pages.component';

const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      {
        path: '',
        loadChildren: './main/main.module#MainModule'
      },
      {
        path: 'services',
        loadChildren: './services/services.module#ServicesModule'
      },
      {
        path: 'login',
        loadChildren: './authentication/login/login.module#LoginModule'
      },
      {
        path: 'user',
        loadChildren: './user/user.module#UserModule'
      },
      {
        path: 'join',
        loadChildren: './common/join/join.module#JoinModule'
      },
      {
        path: 'contractor',
        loadChildren: './contractor/contractor.module#ContractorModule'
      },
      {
        path: 'help',
        loadChildren: './help/help.module#HelpModule'
      },
      {
        path: 'pricing',
        loadChildren: './pricing/pricing.module#PricingModule'
      },
      {
        path: 'become-partner',
        loadChildren: './become-partner/become-partner.module#BecomePartnerModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
