import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {PanelService} from "../../../../services/panel.service";
import {HelperService} from "../../../../services/helper.service";

@Component({
  selector: 'app-ratings',
  templateUrl: './ratings.component.html'
})

export class RatingsComponent implements OnInit, OnDestroy{

  ratings: any;
  subscription: Subscription;
  isLoadingRatings = true;

  constructor(
    private panelService: PanelService,
    private helperService: HelperService
  ) {}

  ngOnInit() {
    this.subscription = this.panelService.fetchRatings().subscribe(res => {
      this.isLoadingRatings = false;
      this.ratings = res;
    });
  }

  formatDate(unixDate) {
    return this.helperService.formatDate(unixDate);
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
