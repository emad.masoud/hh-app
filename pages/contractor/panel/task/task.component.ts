import {Component, OnDestroy, OnInit} from '@angular/core';
import {forkJoin, Subscription} from "rxjs";
import {PanelService} from "../../../../services/panel.service";
import {HelperService} from "../../../../services/helper.service";

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['/task.component.scss']
})

export class TaskComponent implements OnInit {
  jobList = [];
  jobsCalender = [];
  subscription: Subscription;
  isLoadingJobList = true;
  multiDates: Date[] = [];

  constructor(
    private panelService: PanelService,
    private helperService: HelperService
  ) {}

  ngOnInit() {
    forkJoin(
      this.panelService.fetchUpComingJobList(),
      this.panelService.fetchTasksCalender()
    ).subscribe(res => {
      this.jobList = res[0];
      this.jobsCalender = this.jobsCalender.concat(res[1].upcomingJobs);
      this.jobsCalender = this.jobsCalender.concat(res[1].pastJobs);
      this.jobsCalender = this.jobsCalender.concat(res[1].allJobs);
      this.jobsCalender.forEach(jobDate => {
        this.multiDates.push(new Date(jobDate * 1000));
      });
      this.isLoadingJobList = false;
    });
  }

  formatDate(unixDate) {
    return this.helperService.formatDate(unixDate);
  }

  formatTime(unixDate) {
    return this.helperService.formatTime(unixDate);
  }
}
