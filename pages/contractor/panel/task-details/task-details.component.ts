import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { } from 'googlemaps';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '../../../../services/user.service';
import {HelperService} from '../../../../services/helper.service';
import {Subscription} from 'rxjs';
import {PanelService} from '../../../../services/panel.service';

@Component({
  selector: 'app-task-details',
  templateUrl: './task-details.component.html'
})
export class TaskDetailsComponent implements OnInit, OnDestroy {
  @ViewChild('gmap') gmapElement: any;
  map: google.maps.Map;

  taskId: string;
  taskDetails: any;
  subscription: Subscription;
  isLoadingTaskDetails = true;

  constructor(
    private route: ActivatedRoute,
    private panelService: PanelService,
    private helperService: HelperService
  ) {}

  ngOnInit() {
    this.taskId = this.route.snapshot.paramMap.get('taskId');
    this.subscription = this.panelService.fetchTaskDetails(this.taskId).subscribe(res => {
      this.taskDetails = res;
      this.isLoadingTaskDetails = false;
      const mapProp = {
        center: new google.maps.LatLng(this.taskDetails.latitude, this.taskDetails.longitude),
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);
    });
  }

  formatDate(unixDate) {
    return this.helperService.formatDate(unixDate);
  }

  formatTime(unixDate) {
    return this.helperService.formatTime(unixDate);
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
