import {Component} from '@angular/core';

@Component({
  selector: 'app-contractor-sidebar',
  templateUrl: './contractor-sidebar.component.html'
})

export class ContractorSidebarComponent {

  sideBarOpen = true;

  toggleSideBar() {
   this.sideBarOpen = !this.sideBarOpen;
  }
}
