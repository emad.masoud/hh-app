import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {PanelService} from "../../../../services/panel.service";
import {FormControl, FormGroup, FormBuilder, FormArray, Validators} from "@angular/forms";
import {UserService} from "../../../../services/user.service";
import {UserProfile} from "../../../../models/user.model";
import {ToastrService} from "ngx-toastr";
import {HelperService} from "../../../../services/helper.service";

@Component({
  selector: 'app-settings',
  templateUrl: './profile.component.html',
  styleUrls: ['/profile.component.scss']
})

export class ProfileComponent implements OnInit, OnDestroy {

  isValidPhoneNumber = true;
  profileFormSubmit = false;
  uploadingImage = false;
  profileForm = new FormGroup({
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    phoneNumber: new FormControl('', Validators.required),
    aptNumber: new FormControl('', Validators.required),
    streetAddress: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required),
    province: new FormControl('', Validators.required),
    postalCode: new FormControl('', Validators.required),
  });

  experienceForm: FormGroup;
  aboutForm = new FormGroup({
    bio: new FormControl('')
  });

  contractorData: any;
  skills = [];
  certificates = [];
  subscription: Subscription;
  uploadImageSubscription: Subscription;
  isLoadingProfile = true;

  constructor(
    private panelService: PanelService,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private toastrService: ToastrService,
    private helperService: HelperService
  ) {}

  ngOnInit() {
    this.subscription = this.panelService.fetchContractorDetails().subscribe(res => {
      this.contractorData = res;
      this.skills = res.servicesOffered;
      this.certificates = res.certificates;
      this.isLoadingProfile = false;
      this.profileForm.setValue({
        firstName: res.firstName,
        lastName: res.lastName,
        email: res.email,
        phoneNumber: res.phoneNumber,
        aptNumber: res.aptNumber,
        streetAddress: res.streetAddress,
        city: res.city,
        province: res.province,
        postalCode: res.postalCode
      });
      this.profileForm.get('email').disable();
      this.initExperienceFromGroup();
    });
  }

  saveProfile() {
    this.profileFormSubmit = true;
    if (this.profileForm.invalid) { return; }
    this.subscription = this.userService.saveUserProfile(this.profileForm.value).subscribe(res => {
      this.contractorData.firstName = this.profileForm.value.firstName;
      this.contractorData.lastName = this.profileForm.value.lastName;
    });
  }

  get fp() { return this.profileForm.controls; }

  validatePhoneNumber(phoneNumber) {
    if (this.helperService.validatePhoneNumber(phoneNumber)) {
      this.isValidPhoneNumber = true;
      return true;
    } else {
      this.isValidPhoneNumber = false;
      return false;
    }
  }

  initExperienceFromGroup() {
    this.experienceForm = this.formBuilder.group({
      experience: this.contractorData.experience,
      certificates: this.formBuilder.array([])
    });
    this.certificates.forEach(certificate => {
      (this.experienceForm.controls.certificates as FormArray).push(this.formBuilder.group(certificate));
    });
    this.aboutForm.setValue({bio: this.contractorData.bio});
  }

  experience() {
    return this.formBuilder.group({
      name: '',
      institutionName: '',
      category: '',
      year: ''
    });
  }

  addExperience() {
    let length = (this.experienceForm.controls.certificates as FormArray).length;
    if (length === 10) {
      this.toastrService.info('Cannot add more than 10 certificates');
      return;
    }
    (this.experienceForm.controls.certificates as FormArray).push(this.experience());
  }

  removeExperience(index) {
    (this.experienceForm.controls.certificates as FormArray).removeAt(index);
  }

  saveExperience() {
    this.subscription = this.panelService.saveExperienceCertificates(this.experienceForm.value).subscribe(res => {
      return;
    });
  }

  saveAbout() {
    this.subscription = this.panelService.saveAbout(this.aboutForm.value).subscribe(res => {
      return;
    });
  }

  uploadProfileImage(event) {
    if (event.target.files && event.target.files.length) {
      this.uploadingImage = true;
      const image = event.target.files[0];
      this.uploadImageSubscription = this.userService.uploadImage(image).subscribe(res => {
        this.userService.saveUserProfile({profileImage: res.url}).subscribe(result => {
          this.contractorData.profileImage = res.url;
          this.uploadingImage = false;
        });
      });
    }
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.uploadImageSubscription) {
      this.uploadImageSubscription.unsubscribe();
    }
  }
}
