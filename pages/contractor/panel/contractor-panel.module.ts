import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { GenericContentLoaderModule } from '../../../components/generic-content-loader/generic-content-loader.module';

import { ContractorPanelComponent } from './contractor-panel.component';
import { HomeComponent } from './home/home.component';
import { ContractorSidebarComponent } from './contractor-sidebar/contractor-sidebar.component';
import { TaskComponent } from './task/task.component';
import { EarningsComponent } from './earnings/earnings.component';
import { RatingsComponent } from './ratings/ratings.component';
import { PastWorksComponent } from './past-works/past-works.component';
import { OffersComponent } from './offers/offers.component';
import { ProfileComponent } from './profile/profile.component';
import { HelpComponent } from './help/help.component';
import { AboutComponent } from './about/about.component';
import { HighchartsChartModule } from 'highcharts-angular';
import { TaskDetailsComponent } from './task-details/task-details.component';
import {ReactiveFormsModule} from '@angular/forms';
import {NgxSkeletonLoaderModule} from 'ngx-skeleton-loader';
import { FormsModule } from '@angular/forms';
import { FlatpickrModule } from 'angularx-flatpickr';

const routes: Routes = [
  {
    path: '',
    component: ContractorPanelComponent,
    children: [
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'tasks',
        component: TaskComponent
      },
      {
        path: 'earnings',
        component: EarningsComponent
      },
      {
        path: 'ratings',
        component: RatingsComponent
      },
      {
        path: 'past-works',
        component: PastWorksComponent
      },
      /*{
        path: 'offers',
        component: OffersComponent
      },*/
      {
        path: 'profile',
        component: ProfileComponent
      },
      {
        path: 'help',
        component: HelpComponent
      },
      /*{
        path: 'about',
        component: AboutComponent
      },*/
      {
        path: 'task-details/:taskId',
        component: TaskDetailsComponent
      }
    ]
  }
];

@NgModule({
  declarations: [
    ContractorPanelComponent,
    HomeComponent,
    ContractorSidebarComponent,
    TaskComponent,
    EarningsComponent,
    RatingsComponent,
    PastWorksComponent,
    OffersComponent,
    ProfileComponent,
    HelpComponent,
    AboutComponent,
    TaskDetailsComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgxSkeletonLoaderModule,
    RouterModule.forChild(routes),
    HighchartsChartModule,
    FormsModule,
    GenericContentLoaderModule,
    FlatpickrModule.forRoot()

  ]
})
export class ContractorPanelModule { }
