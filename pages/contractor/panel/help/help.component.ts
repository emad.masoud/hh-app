import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {PanelService} from "../../../../services/panel.service";

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html'
})

export class HelpComponent implements OnInit, OnDestroy {
  faqs = [];
  subscription: Subscription;
  isLoadingFaqs = true;

  constructor(
    private panelService: PanelService
  ) {}

  ngOnInit() {
    this.subscription = this.panelService.fetchFaqs().subscribe(res => {
      this.isLoadingFaqs = false;
      this.faqs = res;
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
