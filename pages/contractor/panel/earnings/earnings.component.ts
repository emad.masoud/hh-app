import {Component, OnDestroy, OnInit} from '@angular/core';
import * as Highcharts from 'highcharts';
import {forkJoin, Subscription} from "rxjs";
import {Router} from "@angular/router";
import {HelperService} from "../../../../services/helper.service";
import {PanelService} from "../../../../services/panel.service";

@Component({
  selector: 'app-earnings',
  templateUrl: './earnings.component.html'
})

export class EarningsComponent implements OnInit, OnDestroy{
  Highcharts = Highcharts;
  chartOptions = {
    chart: {
      type: 'column'
    },
    title: {
      text: 'Earnings by month'
    },
    xAxis: {
      categories: [
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'May',
        'Jun',
        'Jul',
        'Aug',
        'Sep',
        'Oct',
        'Nov',
        'Dec'
      ], title: {
        text: null
      }
    },
    yAxis : {
      min: 0, title: {
        text: null
      },
      labels: {
        overflow: 'justify'
      }
    },
    tooltip : {
      valueSuffix: ' $'
    },
    plotOptions : {
      bar: {
        dataLabels: {
          enabled: true
        }
      }
    },
    credits: {
      enabled: false
    },
    series: [
      {
        name: 'Earnings',
        data: [],
        color: '#FF8006'
      },

    ]
  };

  subscription: Subscription;
  isEarningsLoading = true;
  totalEarnings = 0;
  monthlyEarnings = [];
  recentEarnings = [];
  withdrawals = [];

  constructor(
    private router: Router,
    private panelService: PanelService,
    private helperService: HelperService
  ) {}

  ngOnInit() {
    forkJoin(
      this.panelService.fetchEarnings(),
      this.panelService.fetchWithdrawals()
    ).subscribe(res => {
      this.totalEarnings = res[0].totalEarnings;
      this.monthlyEarnings = res[0].monthlyEarnings;
      this.monthlyEarnings.forEach(earnings => {
        this.chartOptions.series[0].data.push(earnings.contractorsMonthlyEarning);
      });
      this.recentEarnings = res[0].recentEarnings;
      this.withdrawals = res[1];
      this.isEarningsLoading = false;
    });
  }

  formatDate(unixDate) {
    return this.helperService.formatDate(unixDate);
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
