import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {PanelService} from "../../../../services/panel.service";
import {HelperService} from "../../../../services/helper.service";

@Component({
  selector: 'app-past-works',
  templateUrl: './past-works.component.html'
})

export class PastWorksComponent implements OnInit, OnDestroy {
  jobList = [];
  subscription: Subscription;
  isLoadingJobList = true;

  constructor(
    private panelService: PanelService,
    private helperService: HelperService
  ) {}

  ngOnInit() {
    this.subscription = this.panelService.fetchPastJobList().subscribe(res => {
      this.isLoadingJobList = false;
      this.jobList = res;
    });
  }

  formatDate(unixDate) {
    return this.helperService.formatDate(unixDate);
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
