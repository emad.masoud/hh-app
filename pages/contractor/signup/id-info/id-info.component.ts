import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {UserService} from "../../../../services/user.service";
import {Subscription} from "rxjs";
import {HelperService} from "../../../../services/helper.service";

@Component({
  selector: 'app-id-info',
  templateUrl: './id-info.component.html'
})
export class IdInfoComponent implements OnInit, OnDestroy {

  uploadingFront = false;
  uploadingBack = false;
  submit = false;
  idCardImageFrontUrl: string;
  idCardImageBackUrl: string;
  subscription: Subscription;

  constructor(
    private router: Router,
    private userService: UserService,
    private helperService: HelperService
  ) {}

  ngOnInit() {
    return this.helperService.handleContractorSignUpSteps(5);
  }

  uploadID(files: FileList, type) {
    if (!files.length) { return; }
    if (type === 'front') {
      this.uploadingFront = true;
    } else {
      this.uploadingBack = true;
    }
    const file = files.item(0);
    this.subscription = this.userService.uploadImage(file).subscribe(res => {
      if (type === 'front') {
        this.idCardImageFrontUrl = res.url;
        this.uploadingFront = false;
      } else {
        this.idCardImageBackUrl = res.url;
        this.uploadingBack = false;
      }
    });
  }

  saveIDInfo() {
    this.submit = true;
    if (!this.idCardImageFrontUrl || this.uploadingBack) { return; }
    let signUpData = JSON.parse(localStorage.getItem('contractorSignUp'));
    signUpData.stepCompleted = 5;
    signUpData.idCardImageFrontUrl = this.idCardImageFrontUrl;
    signUpData.idCardImageBackUrl = this.idCardImageFrontUrl || '';
    localStorage.setItem('contractorSignUp', JSON.stringify(signUpData));
    return this.router.navigate(['/contractor/sign-up/services-info']);
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
