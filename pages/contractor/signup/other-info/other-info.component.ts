import {Component, OnInit} from '@angular/core';
import { FormControl, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import {HelperService} from "../../../../services/helper.service";

@Component({
  selector: 'app-other-info',
  templateUrl: './other-info.component.html'
})
export class OtherInfoComponent implements OnInit {

  otherInfoForm = new FormGroup({
    planOn: new FormControl('1'),
    commuting: new FormControl('1')
  });

  constructor(
    private router: Router,
    private helperService: HelperService
  ) {}

  ngOnInit() {
    return this.helperService.handleContractorSignUpSteps(3);
  }

  saveOtherInfo() {
    let signUpData = JSON.parse(localStorage.getItem('contractorSignUp'));
    signUpData.stepCompleted = 3;
    localStorage.setItem('contractorSignUp', JSON.stringify({...this.otherInfoForm.value, ...signUpData}));
    return this.router.navigate(['/contractor/sign-up/address-info']);
  }
}
