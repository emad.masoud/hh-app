import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import {FormControl, FormGroup, Validators} from "@angular/forms";

import { HelperService } from "../../../../services/helper.service";

@Component({
  selector: 'app-consent',
  templateUrl: './consent.component.html'
})
export class ConsentComponent implements OnInit {

  workInfoFormSubmit = false;
  workInfoForm = new FormGroup({
    experience: new FormControl('', Validators.required),
    isLegalInCanada: new FormControl('false', Validators.required),
    aboutHuntHandy: new FormControl('', Validators.required)
  });

  constructor(
    private router: Router,
    private helperService: HelperService
  ) {}

  ngOnInit() {
    //return this.helperService.handleContractorSignUpSteps(2);
  }

  saveWorkInfo() {
    this.workInfoFormSubmit = true;
    if (this.workInfoForm.invalid) { return; }

    let signUpData = JSON.parse(localStorage.getItem('contractorSignUp'));
    signUpData.stepCompleted = 2;
    localStorage.setItem('contractorSignUp', JSON.stringify({...this.workInfoForm.value, ...signUpData}));
    return this.router.navigate(['/contractor/sign-up/other-info']);
  }

  get f() { return this.workInfoForm.controls; }
}
