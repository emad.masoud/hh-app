import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { TagInputModule } from 'ngx-chips';


import { PersonalInfoComponent } from './personal-info/personal-info.component';
import { WorkInfoComponent } from './work-info/work-info.component';
import { OtherInfoComponent } from './other-info/other-info.component';
import { AddressInfoComponent } from './address-info/address-info.component';
import { IdInfoComponent } from './id-info/id-info.component';
import { ServicesInfoComponent } from './services-info/services-info.component';
import {ConsentComponent} from "./consent/consent.component";

const routes: Routes = [
  {
    path: 'personal-info',
    component: PersonalInfoComponent
  },
  {
    path: 'work-info',
    component: WorkInfoComponent
  },
  {
    path: 'other-info',
    component: OtherInfoComponent
  },
  {
    path: 'address-info',
    component: AddressInfoComponent
  },
  {
    path: 'id-info',
    component: IdInfoComponent
  },
  {
    path: 'services-info',
    component: ServicesInfoComponent
  },
  {
    path: 'consent',
    component: ConsentComponent
  }
];

@NgModule({
  declarations: [
    PersonalInfoComponent,
    WorkInfoComponent,
    OtherInfoComponent,
    AddressInfoComponent,
    IdInfoComponent,
    ServicesInfoComponent,
    ConsentComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    TagInputModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ]
})
export class ContractorSignupModule { }
