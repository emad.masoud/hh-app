import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import { Router } from "@angular/router";
import {HelperService} from "../../../../services/helper.service";

@Component({
  selector: 'app-address-info',
  templateUrl: './address-info.component.html'
})
export class AddressInfoComponent implements OnInit {

  addressInfoFormSubmit = false;
  addressInfoForm = new FormGroup({
    aptNumber: new FormControl('', Validators.required),
    streetAddress: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required),
    province: new FormControl('', Validators.required),
    postalCode: new FormControl('', Validators.required),
  });

  constructor(
    private router: Router,
    private helperService: HelperService
  ) {}

  ngOnInit() {
    return this.helperService.handleContractorSignUpSteps(4);
  }

  saveAddressInfo() {
    this.addressInfoFormSubmit = true;
    if (this.addressInfoForm.invalid) { return; }

    let signUpData = JSON.parse(localStorage.getItem('contractorSignUp'));
    signUpData.stepCompleted = 4;
    localStorage.setItem('contractorSignUp', JSON.stringify({...this.addressInfoForm.value, ...signUpData}));
    return this.router.navigate(['/contractor/sign-up/id-info']);
  }

  get f() { return this.addressInfoForm.controls; }

}
