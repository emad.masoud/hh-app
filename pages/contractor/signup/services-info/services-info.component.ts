import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {Router} from "@angular/router";
import {UserService} from "../../../../services/user.service";
import {ToastrService} from "ngx-toastr";
import {HelperService} from "../../../../services/helper.service";
import {User} from "../../../../models/user.model";

@Component({
  selector: 'app-services-info',
  templateUrl: './services-info.component.html'
})
export class ServicesInfoComponent implements OnInit, OnDestroy {

  submit = false;
  allServices = [];
  selectedService = [];
  subscription: Subscription;

  constructor(
    private router: Router,
    private userService: UserService,
    private toastrService: ToastrService,
    private helperService: HelperService
  ) {}

  ngOnInit() {
    this.subscription = this.userService.fetchServices().subscribe(res => {
      res.forEach(service => {
        this.allServices.push({display: service.name, value: service._id});
      });
    });
    return this.helperService.handleContractorSignUpSteps(6);
  }

  saveServiceInfo() {
    this.submit = true;
    if (!this.selectedService.length) { return; }
    let contractorData = JSON.parse(localStorage.getItem('contractorSignUp')),
      servicesOffered = this.selectedService.map(({value}) => value);
    contractorData.serviceIds = servicesOffered;
    this.subscription = this.userService.contractorSignUp(contractorData).subscribe(res => {
      localStorage.removeItem('contractorSignUp');
      this.submit = false;
      return this.router.navigate(['/']);
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
