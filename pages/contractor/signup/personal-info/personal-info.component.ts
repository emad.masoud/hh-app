import { Component, OnDestroy, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import {HelperService} from "../../../../services/helper.service";

@Component({
  selector: 'app-personal-info',
  templateUrl: './personal-info.component.html'
})
export class PersonalInfoComponent implements OnInit, OnDestroy{

  isValidPhoneNumber = true;
  personalInfoFormSubmit = false;
  personalInfoForm = new FormGroup({
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    phoneNumber: new FormControl('', Validators.required),
    password: new FormControl('', [Validators.required, Validators.minLength(8)]),
    confirmPassword: new FormControl('', Validators.required),
    referralCode: new FormControl('')
  }, this.pwdMatchValidator);

  constructor(
    private router: Router,
    private helperService: HelperService
  ) {}

  ngOnInit() {
    localStorage.removeItem('contractorSignUp');
  }

  savePersonalInfo() {
    this.personalInfoFormSubmit = true;
    if (!this.validatePhoneNumber(this.personalInfoForm.value.phoneNumber)) {
      this.isValidPhoneNumber = false;
      return;
    }
    if (this.personalInfoForm.invalid) { return; }

    localStorage.setItem('contractorSignUp',
      JSON.stringify({ stepCompleted: 1, ...this.personalInfoForm.value })
    );
    return this.router.navigate(['/contractor/sign-up/work-info']);
  }

  get f() { return this.personalInfoForm.controls; }

  pwdMatchValidator(frm: FormGroup) {
    return frm.get('password').value === frm.get('confirmPassword').value
      ? null : { mismatch: true };
  }

  validatePhoneNumber(phoneNumber) {
    if (this.helperService.validatePhoneNumber(phoneNumber)) {
      this.isValidPhoneNumber = true;
      return true;
    } else {
      this.isValidPhoneNumber = false;
      return false;
    }
  }

  ngOnDestroy() {}

}
