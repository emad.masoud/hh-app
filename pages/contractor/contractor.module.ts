import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { ContractorAuthGuard } from "../../guards/contractorAuth.guard";


const routes: Routes = [
  {
    path: 'sign-up',
    loadChildren: './signup/contractor-signup.module#ContractorSignupModule'
  },
  {
    path: 'panel',
    loadChildren: './panel/contractor-panel.module#ContractorPanelModule',
    canActivate: [ ContractorAuthGuard ]
  }
];

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class ContractorModule { }
