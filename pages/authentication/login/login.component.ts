import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService, FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";


import { UserService } from '../../../services/user.service';

import { User } from '../../../models/user.model';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['/login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  email: string;
  password: string;
  userType = 'user';
  loginForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(8)]),
    userType: new FormControl('user')
  });
  loginFormSubmit = false;

  subscription: Subscription;

  constructor(
    private userService: UserService,
    private router: Router,
    private formBuilder: FormBuilder,
    private authService: AuthService
  ) {}

  ngOnInit() {
    const user = JSON.parse(localStorage.getItem('user'));
    if (user) {
      this.subscription = this.userService.user().subscribe(res => {
        if (res) {
          return this.router.navigateByUrl('/');
        }
      });
    }
  }

  login() {
    this.loginFormSubmit = true;
    if (this.loginForm.invalid) { return; }
    this.subscription = this.userService.login(this.loginForm.value).subscribe((res: User) => {
      return this.router.navigateByUrl('/');
    });
  }

  get f() { return this.loginForm.controls; }

  register() {
    const route = this.loginForm.get('userType').value === 'user' ? ['/user/sign-up'] : ['/contractor/sign-up/personal-info'];
    return this.router.navigate(route);
  }

  /*signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }*/

  socialLogin(type: string) {
    let provider = null;
    if (type === 'fb') {
      provider = FacebookLoginProvider.PROVIDER_ID;
    } else {
      provider = GoogleLoginProvider.PROVIDER_ID;
    }

    this.authService.signIn(provider).then(res => {
      console.log(res);
    });
  }

  signOut(): void {
    this.authService.signOut();
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
