export interface Response {
  success: number;
  message: string;
  data: any;
  response: number;
}
export interface Data {
  data: any;
}
